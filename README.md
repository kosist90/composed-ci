# composed-ci

composed-ci takes the LabVIEW-specific configuration complexities out of your continuous integration. 

This package exists because of the following premises:
1. A LabVIEW project should know how to test and build itself. 
2. Configuration and testing of the LabVIEW-specific test and build steps should be able to be performed in the LabVIEW IDE with an equivalent command line API available for the CI server.

By managing LabVIEW&#39;s test-and-build configuration in the project, the API and number of parameters the CI server needs to manage are minimized, which decouples projects from the CI server.  This assists with configuring the CI server by removing responsibilities, and minimizes the amount of information that the CI server has to know about your project.

Test-and-build parameters are stored in a configuration file that lives next to your source code.
Includes a user interface to select which build specifications and tests to run.  

This project uses VI Analyzer and JKI VI Tester.

This package depends on the following VI Packages (all available on VIPM):
LabVIEW CLI by Wiresmith Technology
JKI_labs_tool_vi_tester by JKI
VI Tester JUnit XML Test Results by JKI
VI Analyzer Results to Checkstyle XML Format by National Instruments

This package depends on the following NI Products:
VI Analyzer