# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2019-04-25
### Added
- Added a new method called "Recompile Project" that will force a bottom-up recompile on all VIs in a project, including GPM dependencies.  This will fix any conflicts that may otherwise occur when opening the project and appear in the dependencies section.

## [1.0.1] - 2019-04-17
### Changed
- fixed an issue where dependency order sorting in the relink step did not work for some packages

## [1.0.0] - 2019-04-15
### Changed
- removed a breakpoint from manual build step that prevented builds from working
- updated default value of build target to be 'my computer'
- build step now creates zip files of build output
- build output is left in place, in case it is needed by subsequent build specs
- 'build specification names' no longer builds all if left blank.  Builds must be selected using the 'select build specs' button on the configure build UI

## [0.9.17] - 2019-02-28
### Changed
- manual API methods now generate usefult errors if input paramaters are bad
- improved test coverage for bad of missing configuration files
- improved test coverage for bad or missing working directory
- improved test coverage for VI tester steps, which must be run manually

## [0.9.16] - 2019-02-28
### Changed
- renamed composed-ci.ini to composed-ci.cfg
- added 'select build spec' button to UI
- added 'select tests' button to UI and reworked the VI tester operation to use a folder and whitelist/blacklist
### Removed
- removed pipeline build step

## [0.9.14] - 2019-02-26
### Changed
- removed dependency on ci-common-steps
- updated documentation and icon/avatar
- Open Project from CLI will return an error if <1 good VIs found or >0 bad VIs

## [0.9.12] - 2019-02-25
### Changed
- small refactor from VIA feedback
- added mass compile and relink steps to the CLI API
significant rework of the sweet user interface, including new buttons for mass compile and relinking

## [0.9.11] - 2019-02-19
### Changed
- updated composed-ci.ini
- removed configuration.lvlib
- removed dependency on @cs/configuration
- added open project step
- refactor due to new VIA configuration

## [0.1.0] - 2019-02-7
### Added
Initial release

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security